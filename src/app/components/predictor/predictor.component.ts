import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { appConstants } from '../../constants/appConstants';
import { ToastrService } from 'ngx-toastr';
import { element } from 'protractor';
import { RestrictionDayModel } from './models/restrictionDayModel';
import { PredictorService } from './services/predictor.service';
import { RestrictionTimeModel } from './models/restrictionTimeModel';

@Component({
  selector: 'app-predictor',
  templateUrl: './predictor.component.html',
  styleUrls: ['./predictor.component.css']
})
export class PredictorComponent implements OnInit {

  public picoPlacaForm = new FormGroup({
    plateNumber: new FormControl('', [Validators.required, Validators.minLength(7), Validators.maxLength(7), Validators.pattern(appConstants.PLATE_NUMBER_PATTERN)]),
    date: new FormControl('', [Validators.required, Validators.pattern(appConstants.DATE_PATTERN)]),
    time: new FormControl('', [Validators.required, Validators.pattern(appConstants.TIME_PATTERN)])
  });
  public validDate: boolean = true;
  public invalidDateMsg = null;
  public validPlateNumber: boolean = true;
  public invalidPlateNumberMsg = null;
  private dayOfWeek: number;
  public restrictionHours:Array<RestrictionTimeModel>;
  public noRestriction:boolean;
  public restriction:boolean;
  public onWeekend:boolean;
  public noTimeRestriction: boolean;

  constructor(private toastr: ToastrService, private predictorService: PredictorService) { }

  ngOnInit(): void {
    this.picoPlacaForm.controls.date.valueChanges.subscribe(() => {
      this.validateDate();
    });
    this.picoPlacaForm.controls.plateNumber.valueChanges.subscribe(() => {
      this.validatePlateNumber();
    });
    this.restrictionHours = this.predictorService.getRestrictionHours();
  }

  verify() {
    this.picoPlacaForm.markAllAsTouched();
    this.resetView()
    if (this.picoPlacaForm.valid && this.validDate && this.validPlateNumber) {
      this.verifyLastDigitRestriction();
    } else if (!this.validPlateNumber) {
      this.toastr.error(this.invalidPlateNumberMsg);
    } else if (!this.validDate) {
      this.toastr.error(this.invalidDateMsg);
    }
  }

  validateDate() {
    let dateValues: Array<number> = this.picoPlacaForm.controls.date.value.split('/');
    let parsedDate: Date = new Date(dateValues[2], dateValues[1] - 1, dateValues[0]);
    let inputDay = Number(dateValues[0]);
    if (parsedDate.getDate() != inputDay) {
      let inputMonth = Number(dateValues[1]);
      this.invalidDateMsg = 'Inavlid date, ' + appConstants.MONTHS[inputMonth - 1] + ' doesn\'t have ' + inputDay + ' days.';
      this.validDate = false;
    } else {
      this.invalidDateMsg = null;
      this.validDate = true;
      this.dayOfWeek = parsedDate.getDay();
    }
  }

  validatePlateNumber() {
    if (this.picoPlacaForm.controls.plateNumber.value.length > 0) {
      let plateInitial = this.picoPlacaForm.controls.plateNumber.value.charAt(0);
      let foundInitial = appConstants.PLATE_INITIALS.find(element => element == plateInitial)
      if (foundInitial == undefined) {
        this.invalidPlateNumberMsg = `Plate number with intial letter ${plateInitial} doesn't exist.`;
        this.validPlateNumber = false;
      } else {
        this.invalidPlateNumberMsg = null;
        this.validPlateNumber = true;
      }
    }
  }

  upperCase() {
    let val = this.picoPlacaForm.controls.plateNumber.value;
    this.picoPlacaForm.controls.plateNumber.setValue(val.toUpperCase())
  }

  verifyLastDigitRestriction() {
    let restrictionDays:Array<RestrictionDayModel> = this.predictorService.getRestrictionDays();
    let foundRestrictionObj = restrictionDays.find(element => element.day == this.dayOfWeek);
    if (foundRestrictionObj != undefined) {
      let plateLastDigit = this.picoPlacaForm.controls.plateNumber.value.charAt(6);
      let foundRestrictionNumber = foundRestrictionObj.restrictions.find(elem => elem == plateLastDigit);
      if (foundRestrictionNumber != undefined) {
        let existTimeRestriction = this.verifyTimeRestriction();
        if(existTimeRestriction){
          this.restriction = true;
        } else {
          this.noTimeRestriction = true;
        }
      } else {
        this.noRestriction = true;
      }
    } else {
      // weekend
      this.onWeekend = true;
    }
  }

  verifyTimeRestriction(){
    let restriction = false;
    for(var i=0; i <this.restrictionHours.length; i++){
      let splitTime:string[] = this.picoPlacaForm.controls.time.value.split(':');
      let dateInpt = new Date();
      dateInpt.setHours(Number(splitTime[0]), Number(splitTime[1]));
      if(dateInpt >= this.restrictionHours[i].start && dateInpt <= this.restrictionHours[i].end){
        restriction = true;
      }
    }
    return restriction;
  }

  resetView(){
    this.restriction = false;
    this.noRestriction = false;
    this.onWeekend = false;
    this.noTimeRestriction = false;
  }

}
