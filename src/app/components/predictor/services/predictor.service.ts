import { Injectable } from '@angular/core';
import { RestrictionDayModel } from '../models/restrictionDayModel';
import { RestrictionTimeModel } from '../models/restrictionTimeModel';

@Injectable({
  providedIn: 'root'
})
export class PredictorService {

  constructor() { }

  getRestrictionDays(){
    let restrictionDays:Array<RestrictionDayModel> = [];
    restrictionDays.push(new RestrictionDayModel(1, [1,2]));
    restrictionDays.push(new RestrictionDayModel(2, [3,4]));
    restrictionDays.push(new RestrictionDayModel(3, [5,6]));
    restrictionDays.push(new RestrictionDayModel(4, [7,8]));
    restrictionDays.push(new RestrictionDayModel(5, [9,0]));
    return restrictionDays;
  }

  getRestrictionHours():Array<RestrictionTimeModel>{
    let restrictionHours:Array<RestrictionTimeModel> = [];
    let date1 = new Date();
    date1.setHours(7, 0);
    let date2 = new Date();
    date2.setHours(9, 30);
    restrictionHours.push(new RestrictionTimeModel(date1, date2));
    let date3 = new Date();
    date3.setHours(16, 0);
    let date4 = new Date();
    date4.setHours(19, 30);
    restrictionHours.push(new RestrictionTimeModel(date3, date4));
    return restrictionHours;
  }
}
