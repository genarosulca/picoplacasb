export class RestrictionDayModel {
    day?: number;
    restrictions?:number[];
    
    constructor(day:number, restrictions:number[]){
        this.day = day;
        this.restrictions = restrictions;
    }
}
