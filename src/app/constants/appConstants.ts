export abstract class appConstants {
    static readonly DATE_PATTERN: RegExp = /^(0[1-9]|[12][0-9]|3[01])[\/\-](0[1-9]|1[012])[\/\-]\d{4}$/;
    static readonly TIME_PATTERN: RegExp = /^([0-1][0-9]|2[0-3]):([0-5][0-9])(:[0-5][0-9])?$/;
    static readonly PLATE_NUMBER_PATTERN: RegExp = /^[A-Za-z]{3}[0-9]{4}/;
    static readonly MONTHS: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    static readonly PLATE_INITIALS: string[] = ['A', 'B', 'U', 'C', 'X', 'H', 'O', 'E', 'W', 'G', 'I', 'L', 'R', 'M', 'V', 'N', 'S', 'P', 'Q', 'K', 'T', 'Z', 'Y', 'J'];
    // PLATE INITIALS TAKED FROM https://ecuadorec.com/placas-vehiculos-ecuador-tipos-letras-provincia/
}