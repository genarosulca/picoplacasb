import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PredictorComponent } from './components/predictor/predictor.component';

const routes: Routes = [
  { path: 'predictor', component: PredictorComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: 'predictor' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
